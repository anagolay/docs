/** @type {import('tailwindcss').Config} */
module.exports = {
  plugins: [],
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  prefix: 'tw-',
  theme: {
    extend: {
      colors: {
        spaceBlue: {
          50: '#F4FBFF',
          // '100': '#B6E3FC',
          // '200': '#85D1FA',
          // '300': '#54BEF8',
          400: '#23ACF6',
          // '500': '#0992DC',
          600: '#0772AB',
          700: '#054F77',
          800: '#093954',
          900: '#052233',
        },
        neonGreen: {
          // '50': '#F3FFE5',
          // '100': '#DDFFB3',
          // '200': '#C6FF80',
          // '300': '#AFFF4D',
          400: '#8CFF00',
          // '500': '#7EE600',
          // '600': '#62B300',
          // '700': '#468000',
          // '800': '#2A4D00',
          // '900': '#0E1A00',
        },
      },
    },
  },
};
