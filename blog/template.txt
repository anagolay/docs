---
title: Workflows - performance analysis
description: Workflows generation retrospective and performance analysis. One of Anagolay strong points is the capability to generate code by combining Operations into a Workflow, given a manifest that is plain JSON.
authors: [adriano]
tags:
  - process
  - transparent
  - evidence
image: https://ipfs.anagolay.network/ipfs/bafybeih6hpvgetxd5uer6jv3ys3xtoyfrlk2x7okqmm6d4ci7dthigyvzm
date: 2022-07-14
---