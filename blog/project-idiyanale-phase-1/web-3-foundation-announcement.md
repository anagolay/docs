---
title: Idiyanale P1 - Announcement
description: Anagolay team has finalized the last milestone of Project Idiyanale Phase1 under the Web3 Foundation Grant Program.
authors: [daniel]
tags:
  - web-3-foundation
  - idiyanale
  - grant
image: https://ipfs.anagolay.network/ipfs/bafybeih6hpvgetxd5uer6jv3ys3xtoyfrlk2x7okqmm6d4ci7dthigyvzm
date: 2022-06-15
---

![idiyanale-phase-1-w3f-announcement](https://ipfs.anagolay.network/ipfs/QmUA2GCw44n9oVgMmREAVywSNXoBgA298AUHQLuQfbdcaR?filename=idiyanale-phase-1-w3f-announcement.png)

Anagolay team has finalized the last milestone of Project Idiyanale Phase1 under the Web3 Foundation Grant Program.

The PR for the deliverable - [https://github.com/w3f/Grant-Milestone-Delivery/pull/453](https://github.com/w3f/Grant-Milestone-Delivery/pull/453)

Accepted Grant Proposal - [https://github.com/w3f/Grants-Program/pull/719](https://github.com/w3f/Grants-Program/pull/719)

Anagolay is a next-generation framework for ownerships, copyrights, and digital licenses. We use Substrate to create transparent processes for the creation & validation of proofs and statements of rights.

<!--truncate-->

_Idiyanale chain_ can be considered a canary Network of Anagolay, but it is [much more than that](https://www.notion.so/Anagolay-FAQ-530c3d1de6784e7eb0c842c7b9bedfa9). Initially, it is intended as a playground for developers to try and test both new and existing features of Anagolay Network in an unrefined, unpredictable but realistic on-chain environment.

The first phase of Idiyanale implementation, as outlined in this Web3 Foundation Grant [**proposal**](https://github.com/w3f/Grants-Program/pull/719) focuses on the implementation of the core building blocks of Anagolay: **[Operations](./milestone-1.md#operation-environment)** and **[Workflows](./milestone-2.md#workflow----auditable-transparent-and-trusted-process).**

## Why does Web3 need Workflows?

It’s [established](https://ethereum.org/en/web3/#web3) that the concept of individual user ownership over data and content created is the defining characteristic of Web 3.0. The third generation of the internet is supposed to be **[read-write-own](https://twitter.com/himgajria/status/1266415636789334016).** From the very first distributed ledger, to smart contracts and to, ultimately, NFTs (in their broader sense), the teams have been working to establish ownership over certain resources (or assets) in a decentralized trustless environment. However, the existing solutions still have significant limitations when it comes to:

1. reliably proving ownership (unless it’s vetted by a centralized party)
2. efficiency & scalability
3. providing a flexible approach to cover a variety of real-life scenarios

Anagolay is a framework for the creation of deterministic, tamper-proof, impartial, and transparent processes (a.k.a. workflows) that can be applied to verify the rightfulness of a claim for any relation or right, including complex rights management cases.

Anagolay associates several identifiers of authenticity (we call them Proofs) and allows to verify the correctness of a claim against such identifiers. Computing the identifiers is a repeatable process that always returns the same output no matter where or when the computation executes as long as the user provides the same input data. The execution consists of several tasks, called Operations. When connected together, they make up a Workflow.

## Idiyanale Phase 1 Deliverables

In the first milestone [deliverable,](https://github.com/w3f/Grant-Milestone-Delivery/pull/388) we explained the importance of _rights & restrictions_ in the distributed web and outlined Anagolay's approach to rights management. More details about this deliverable are in [this blogpost](./milestone-1.md).

Milestone 2 brings plenty of new stuff: WebSocket microservice, brand new UI for workflow building, two operations, a new publisher job and rust code generation for workflows, deterministic build of WASM artifacts, and two demo applications. For details please see the [Milestone 2 blogpost](./milestone-2.md).

## Ecosystem relevance

Substrate-based projects also can include and use the Anagolay pallets to store and build the Workflows and Operations or include and use the Anagolay pallets if they wish to maintain the Proofs and Statements for themselves.

Any project can use Idiyanale (and later Anagolay) to:

- define their Workflows which are stored on the Anagolay chain and then execute them on the runtime or in off-chain-workers
- use the Anagolay Workflows on any rust or WASM able runtime

## What’s next?

The next steps — prepare and launch the Idiyanale utility token. Test the capabilities of the framework, and incentivize the community to create and test multiple workflows.

Later on, the Anagolay Network will follow the same structure as Idiyanale and **will start with all the data from Idiyanale Network in its genesis block**.

The team will also focus on scaling up the pilot use case and the first hybrid Dapp on Anagolay — [Kelp.digital](https://kelp.digital/). Kelp MVP was validated in the digital photography market in 2021, however, the possibilities that Anagolay infrastructure provides go far beyond that. Anagolay’s proof-based IP verification technology could be similarly applied in the music industry, video production, streaming applications, textual artworks, or code.

To learn more about [Anagolay.Network](http://Anagolay.Network) join our [Kelp&&Anagolay HQ on Discord](https://discord.gg/75aseEyyvj) or follow us on [Twitter](https://twitter.com/AnagolayNet) or [Matrix](https://matrix.to/#/#anagolay-general:matrix.org).

---

**About**

Anagolay Network is the product of Kelp Digital OÜ, a software development company incorporated in Estonia that works on Anagolay technology and its first commercial use case - Kelp.Digital

Web3 Foundation funds research and development teams building the technology stack of the decentralized web. It was established in Zug, Switzerland by Ethereum co-founder and former CTO Gavin Wood. Polkadot is the Foundation's flagship project
