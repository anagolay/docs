---
title: Intro to Anagolay Browser Extension
description: Anagolay Web Extension - Tipping to Verified Domains step-by-step guide
authors: [elena, daniel]
tags:
  - extension
  - browser
  - chromium
  - tutorial
  - step-by-step
image: ./assets/intro-extension-header.png
date: 2023-01-23
---

![intro header](./assets/intro-extension-header.png)

With Anagolay, any verified online identity, like a web domain, Twitter handler, YouTube or GitHub account, or any Mastodon federated identity, can be used to send and receive funds: as community contributions to support a **verified** creator or a particular project.

Here's how it works.

<!--truncate-->

- We've built [Anagolay app](https://app.anagolay.network) to help creators streamline the management of their community subscriptions across platforms where they _already_ share content. Any creator can do so by verifying their channels, a.k.a public identities. The 1st verification strategy we implemented is [web domain verification](https://anagolay.dev/docs/tutorials/prove-domain-ownership/); the strategies for other channels will follow.
  This means now content creators can have multiple revenue streams, circumventing vendor locking by a platform where they built their following. No signup or registration is needed to get started, just a good old Substrate account that you probably already use in multiple networks.
- Once a creator verifies their channel (ex., web domain), they can start receiving contributions from the community. The key here is to make this process as easy as possible - for everyone who wants to support creative work. That's where the *Anagolay browser extension* comes in.
  - No extra steps like registering on yet another platform, no strings attached.
  - The extension works in any (chromium-based) browser.
  - Community and supporters can always check the verification and know for certain who they support.

From the get-go, the tips can be sent in IDI (our native token). However, we are aiming to enable tips in any DotSama token. Later on, the integrations to enable tips in ERC20 tokens and fiat currencies will follow.

Being native to Substrate, Anagolay can bring whole new communities of creators to the ecosystem that were previously wary of the web3 space. In the long run, we see Anagolay as a sound alternative to platforms like Patreon (content creators), and OpenCollective (open-source projects) that combines the best of the web2 and web3 feature sets to deliver the optimal user experience and functionality for creators.

Now that you know the context, let's get started!

:::info
Since Google is still reviewing the extension, you won't find it in the Google store until it is confirmed. For now, it can only be installed manually.
:::

### Download & Extract

1. Download the extension from IPFS using [this link](https://ipfs.anagolay.network/ipfs/bafybeihiz55m6p2ksxhh5jdj2hpqfeohpltiwsg5ud6wk25cslorsq3y4q?filename=anagolay-extension_0.1.0.zip)
2. Save it to the hard drive
3. Extract it to the desired location.

![Extract the zip file](./assets/Untitled.png)
Extract the zip file

## Installation in browsers

Currently available for Chromium-based browsers only (Chrome, Brave, Opera, Edge, etc.). In the Chrome tutorial, you'll find the screenshots of the process; in other browsers, the steps are very similar

### **Chrome**

1. Click on the **Menu** → **More tools** → **Extensions**
2. Enable the **Developer Mode** in the top right corner
3. Click on **Load unpacked** and locate extracted directory. The name should be `anagolay-extension_0.1.0`
4. Double-click on it and then click on the _Select Folder_ at the bottom of the window

   ![install (1).png](<./assets/install_(1).png>)

5. Now you should see the extension on your extension list

   ![install (2).png](<./assets/install_(2).png>)

6. Anagolay Extension will appear in the **Extension dropdown**, click on the pin icon to have it always in the toolbar

   ![install (3).png](<./assets/install_(3).png>)

### **Brave**

1. Click on the **Menu** → **Extensions**
2. Enable the **Developer Mode** in the top right corner
3. Click on **Load unpacked** and locate extracted directory. The name should be `anagolay-extension_0.1.0`
4. Double-click on it and then click on the _Select Folder_ at the bottom of the window
5. Now you should see the extension on your extension list
6. Anagolay Extension will appear in the **Extension dropdown**. Click on the pin icon to have it always in the toolbar

### **Opera**

1. Click on the Opera logo (top left corner) → Extensions → Extensions / Or Ctrl+Shift+E
2. Enable the **Developer Mode** in the top right corner
3. Click on **Load unpacked** and locate extracted directory. The name should be `anagolay-extension_0.1.0`
4. Double-click on it and then click on the _Select Folder_ at the bottom of the window
5. Now you should see the extension on your extension list
6. Anagolay Extension will appear in the **Extension dropdown**, click on the pin icon to have it always in the toolbar

### **Edge**

1. Click on the **Menu** → **Extensions** → **Manage Extensions**
2. Enable the **Developer Mode** in the left sidebar
3. Click on **Load unpacked** and locate extracted directory. The name should be `anagolay-extension_0.1.0`
4. Double-click on it and then click on the _Select Folder_ at the bottom of the window

   ![EDGE browser](./assets/edge-browser.png)

5. Now you should see the extension on your extension list
6. Anagolay Extension will appear in the **Extension dropdown**, click on the pin icon to have it always in the toolbar

Now, when you have the extension, you can create an account or import an existing one from the seed if you have a Substrate account already. See the steps described below.

## Account creation

1. Open the Anagolay Extension
2. Click on **Create** button or the **+** sign and then select **Create new account**
3. Fill in the form
   1. name the account ( this can be done later too )
   2. select checkbox ("I have copied the seed phrase and saved it")
   3. add a password of a minimum of six characters
4. Save the account

![Click on create or +](./assets/Frame_511.png)

Click on create or +

![First make sure to save the seed phrase, then add name and password and save](./assets/Frame_512.png)

First make sure to save the seed phrase, then add name and password and save

## Account import

1. Open the Anagolay Extension
2. Click on the **Import** button or the **+** sign and then select **Import from pre-existing seed**
3. Paste your mnemonic seed first
4. Set the password and add the account name
5. Save

## Getting IDI tokens

_In the alpha stage of the Anagolay Extension, we will send 2 IDI to every person who is willing to test the tipping._

1. Notify us / tag us on [Mastodon](https://mastodon.social/@anagolay) or on [Twitter](https://twitter.com/AnagolayNet) to get a few IDI, and don't forget to follow. 😃
2. Once you've got your IDI tokens, you're all set.

![Not enough funds, please contact us on social media](./assets/Untitled%201.png)

Not enough funds, please contact us on social media

## Sending Tips

As you know, in order to enable tipping, creators need to verify their channel first.

You can, of course, also verify your domain too and enable tips. The process will be covered in a separate tutorial and is shown in this [web domain verifications demo](https://anagolay.dev/docs/tutorials/prove-domain-ownership/). Once verified - don't forget to share with your community 😉.

For the purpose of this tutorial, hre are few domains already verified on Idiyanale so that you can test the tipping functionality right away.

1. Visit one of the verified websites
   1. [https://woss.io](https://woss.io) << verified, tipping enabled
   2. [https://woss.photo](https://woss.photo) << verified, tipping enabled
   3. [https://anagolay.network](https://anagolay.network/) << verified, tipping NOT enabled
   4. [https://anagolay.dev](https://anagolay.dev/) << verified, tipping NOT enabled
   5. [https://kelp.digital](https://kelp.digital) << verified, tipping NOT enabled
2. To send a tip, click on the blue Anagolay logo in the toolbar
3. Select the tip amount or a custom one
4. Click **Send tip**
5. Now you should see the success animation message
6. That's it 🎉

Note: if you want to see the transaction, you can click on the right-bottom link “Transaction Finalized”, and will redirect to the PolkadotJs.

![Select the tip amount and send tip](./assets/Untitled%202.png)

Select the tip amount and send tip

![Tip successfully sent](./assets/Untitled%203.png)

Tip successfully sent!

## Troubleshooting

Open the Extension settings and find the Anagolay Extension. If there is an `Error` button, that means there are errors; if that button doesn't exist, everything is OK.

![Extension with the Error button](./assets/Untitled%204.png)

Extension with the Error button

Clicking on it will open a list of errors. We have here only one for test purposes. In the real world this can have many.

![Error view](./assets/Untitled%205.png)

Error view

Another way to get to the same outcome is to `Right click` on the extension, then click on the `Inspect`.

![Extension popup with Right click menu](./assets/Untitled%206.png)

Extension popup with Right click menu

This will open DevTools, where you should click on `Console`. This will show yo the error as well.

![Untitled](./assets/Untitled%207.png)

We are using a self-hosted instance of error-collecting software called the [sentry](https://sentry.io), which is GDPR compliant. We do not store any information that can uniquely identify a user.

---

## Video tutorials

<iframe width="100%" height="415" src="https://www.youtube.com/embed/uySt392C6TE" title="Intro to Anagolay Browser Extension" frameBorder="0" allow="autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen={true}></iframe>

<iframe width="100%" height="415" src="https://www.youtube.com/embed/dxLi97Nv-KY" title="Enable Tipping in Anagolay App" frameBorder="0" allow="autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen={true}></iframe>

---

## What's next?

Further on, we will be building & rolling out more Verifications Strategies, particularly for Mastodon, Twitter, and YouTube accounts.

To learn more about [Anagolay.Network](https://anagolay.network/) join our [Kelp&&Anagolay HQ on Discord](https://discord.gg/75aseEyyvj) or follow us on [Twitter](https://twitter.com/AnagolayNet) and [Mastodon](https://mastodon.social/@anagolay) or [Matrix](https://matrix.to/#/#anagolay-general:matrix.org).
