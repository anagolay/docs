// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Anagolay and Kelp docs',
  tagline: 'All Anagolay and Kelp related documentation',
  url: 'https://anagolay.dev',
  baseUrl: '/',
  baseUrlIssueBanner: true,

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'https://ipfs.anagolay.network/ipfs/QmQVnJYMMDwXo8MRpaMb998rV6bkYUX1brrpvCLwGVCzLS?filename=favicon.svg',
  trailingSlash: true,
  scripts: [{ src: 'https://analytics.kelp.digital/js/plausible.js', defer: true, 'data-domain': 'anagolay.dev' }],
  markdown: {
    mermaid: true,
  },
  themes: ['@docusaurus/theme-mermaid'],
  plugins: [
    // @ts-ignore
    require('./plugins/gitpod'),
    [
      'ideal-image',
      /** @type {import('@docusaurus/plugin-ideal-image').PluginOptions} */
      ({
        quality: 70,
        max: 1030,
        min: 640,
        steps: 2,
        // Use false to debug, but it incurs huge perf costs
        disableInDev: true,
      }),
    ],
  ],
  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        debug: true, // force debug plugin usage

        docs: {
          // sidebarPath: require.resolve('./sidebars.js'),
          path: 'docs',
          sidebarPath: 'sidebars.js',
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
        },
        blog: {
          path: 'blog',
          feedOptions: {
            type: 'all',
            copyright: `Copyright © ${new Date().getFullYear()} Anagolay Network`,
          },
          blogTitle: 'Anagolay Blog',
          blogDescription: 'Anagolay and Kelp blog, our place to share stuff with you',
          blogSidebarTitle: 'All posts',
          routeBasePath: '/blog',
          include: ['**/*.{md,mdx}'],
          exclude: ['**/_*.{js,jsx,ts,tsx,md,mdx}', '**/_*/**', '**/*.test.{js,jsx,ts,tsx}', '**/__tests__/**'],
          postsPerPage: 10,
          // blogListComponent: '@theme/BlogListPage',
          // blogPostComponent: '@theme/BlogPostPage',
          // blogTagsListComponent: '@theme/BlogTagsListPage',
          // blogTagsPostsComponent: '@theme/BlogTagsPostsPage',
          // rehypePlugins: [],
          // beforeDefaultRemarkPlugins: [],
          // beforeDefaultRehypePlugins: [],
          truncateMarker: /<!--\s*(truncate)\s*-->/,
          showReadingTime: true,
        },
        theme: {
          customCss: [require.resolve('./src/css/custom.css')],
        },
        pages: {},
        sitemap: {
          // Note: /tests/docs already has noIndex: true
          ignorePatterns: ['/tests/{blog,pages}/**'],
        },
      }),
    ],
  ],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        hideOnScroll: true,
        title: 'Anagolay+Kelp=😻',
        logo: {
          alt: 'Anagolay+kelp logo',
          src: 'https://ipfs.anagolay.network/ipfs/QmVpp83HfN3gdhd6fgjQcGqga9yw7kXB7qpmCMWsChnTDP',
          target: '_self',
          width: 32,
          height: 32,
          className: 'tw-rounded-full',
        },
        items: [
          {
            type: 'doc',
            docId: 'README',
            position: 'left',
            label: 'Docs',
          },
          {
            to: 'blog',
            position: 'left',
            label: 'Blog',
          },

          {
            label: 'Testnet Explorer',
            position: 'right',
            href: 'https://polkadot.js.org/apps/?rpc=wss%3A%2F%2Fidiyanale-testnet.anagolay.io',
          },
          {
            label: 'Discord',
            position: 'right',
            href: 'https://discordapp.com/invite/WHe4EuY',
          },

          {
            label: 'Anagolay White Paper',
            position: 'right',
            href: 'https://bit.ly/Anagolay_WhitePaper_wip',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Anagolay Quick Links',
            items: [
              {
                label: 'Docs',
                to: '/docs',
              },
              {
                label: 'JS/TS SDK',
                to: '/docs/anagolay/js-sdk/',
              },
              {
                label: 'Anagolay CLI',
                to: '/docs/anagolay/support/anagolay-cli',
              },
              {
                label: 'Anagolay Explanation Deck',
                position: 'right',
                href: 'https://bit.ly/Anagolay_deck2023Q1',
              },
            ],
          },
          {
            title: 'Kelp Quick Links',
            items: [
              {
                label: 'Macula is missing layer for IPFS',
                to: '/docs/kelp/macula',
              },
              {
                label: 'Remote signer',
                to: '/docs/kelp/remote-signer',
              },
              {
                label: 'Web3 auth',
                to: '/docs/kelp/multi-authentication',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Discord',
                href: 'https://discordapp.com/invite/WHe4EuY',
              },
              {
                label: 'Matrix',
                href: 'https://matrix.to/#/#anagolay-general:matrix.org',
              },
              {
                label: 'Anagolay on Mastodon',
                href: 'https://mastodon.social/@anagolay',
              },
              {
                label: 'Anagolay Twitter',
                href: 'https://twitter.com/AnagolayNet',
              },
              {
                label: 'Idiyanale Twitter',
                href: 'https://twitter.com/IdiyanaleNet',
              },
            ],
          },
          {
            title: 'Websites',
            items: [
              {
                href: 'https://kelp.digital',
                label: 'Kelp Digital',
                position: 'right',
              },
              {
                href: 'https://anagolay.network',
                label: 'Anagolay Network',
                position: 'right',
              },
              {
                href: 'https://github.com/anagolay',
                label: 'Anagolay GitHub',
                position: 'right',
              },
              {
                href: 'https://github.com/kelp-hq',
                label: 'Kelp Digital GitHub',
                position: 'right',
              },
            ],
          },
        ],
        copyright: `Copyright © 2019 - ${new Date().getFullYear()} Kelp Digital OÜ`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
        additionalLanguages: ['rust'],
      },
      algolia: {
        // The application ID provided by Algolia
        appId: 'WN1M3JBJTT',

        // Public API key: it is safe to commit it
        apiKey: '5d7ab70b7e5b236090387b5efbca7879',

        indexName: 'anagolay',

        // Optional: see doc section below
        contextualSearch: true,

        // Optional: Algolia search parameters
        searchParameters: {},

        // Optional: path for search page that enabled by default (`false` to disable it)
        // searchPagePath: 'search',
      },
    }),
};

module.exports = config;
