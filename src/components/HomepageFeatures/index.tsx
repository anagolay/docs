import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

type FeatureItem = {
  title: string;
  Svg: React.ComponentType<React.ComponentProps<'svg'>>;
  description: JSX.Element;
};

const FeatureList: FeatureItem[] = [
  {
    title: 'Source code availability & trustworthiness',
    Svg: require('@site/static/img/source_code.svg').default,
    description: (
      <>
        Highly available storage for source code built with the new ContentVersioning approach gives assurance and
        trustworthiness by design to developers and users alike.
      </>
    ),
  },
  {
    title: 'IP management in the Web3',
    Svg: require('@site/static/img/web3.svg').default,
    description: (
      <>
        While creating & storing records of Rights, Restrictions, and Proofs Anagolay allows asset creators to establish
        how digital assets are used and owned.
      </>
    ),
  },
  {
    title: 'Digital Art P2P Licensing',
    Svg: require('@site/static/img/digital_art.svg').default,
    description: (
      <>
        A meaningful and straightforward way to monetize & buy digital art. From verified creators, with customized
        licensing options. Not just "as a collectible".
      </>
    ),
  },
];

function Feature({ title, Svg, description }: FeatureItem) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
