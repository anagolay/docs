/**
 * Docusaurus plugin to work with the gitpod and HMR
 */
module.exports = function () {
  const { GITPOD_WORKSPACE_URL } = process.env;
  return {
    name: 'enable-hot-reload-for-gitpod',
    configureWebpack(config, isServer) {
      if (isServer) {
      } else {
        return {
          devServer: {
            ...config.devServer,
            compress: true,
            port: 7777,
            client: {
              webSocketURL: GITPOD_WORKSPACE_URL
                ? GITPOD_WORKSPACE_URL.replace('https://', 'wss://7777-') + '/ws'
                : `ws://localhost:7777/ws`,
            },
          },
        };
      }
    },
  };
};
