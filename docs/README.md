---
title: Introduction
description: Welcome to Anagolay and Kelp Documentation website. Here you will find everything you need to know about how to run your Anagolay and Idiyanale node, how to validate them and how to create Copyrights and Ownerships.
slug: /
---

:::tip
Welcome to Anagolay and Kelp Documentation website. Here you will find everything you need to know about how to run your Anagolay and Idiyanale node, how to validate them and how to create Copyrights and Ownerships. Part of the website is dedicated to Kelp Digital products such as [Macula](./kelp/macula.md) and other IPFS-related proxies and a multi-authentication scheme that can authenticate API requests by using web3 addresses and classic [API keys](./kelp/multi-authentication.md).
:::

## What is Anagolay?

Anagolay is a process-based interplanetary P2P framework built for storing, verifying, and transferring Intellectual Property rights like digital Copyrights and Ownerships. The power of Anagolay lies in the Workflows - blockchain agnostic processes built for off-chain use.

Anagolay comes with built-in economic incentives to ensure the Workflows and Operations are correctly implemented, audited and tested by the community.

Anagolay includes a blockchain and native utility token ([AGY](./anagolay/token/agy.md)/[IDI](./anagolay/token/idi.md)). Participants on the network can earn the token by validating the Workflows and Operations or being the chain validator.

## For users

Anagolay brings the traceability of the implemented process for Copyright and Ownership statements.
This means, once you verify your camera, the copyrights are yours, and nobody can take them away.

![Anagolay-Network-Statements&Claims](/assets/statements-and-claims.jpg)

## For developers

Imagine a world where thousands of developers implement the same transparent process for the same or similar input data and get the predictive outputs that can be verified against already existing Proofs and Statements.

## Vision

Our vision is to bring provable statements of copyright and ownership to everyone on the planet.
We create a framework for the creation of deterministic, tamper-proof, impartial, and transparent processes that can be applied to verify the rightfulness of a claim for any relation or right.

Our goal is to enhance trust and value creation over digital creative assets by establishing transparent "rules of the game," where users could not just validate the authenticity of an asset but also verify and exercise the rights associated with it.

We believe the proposed approach enables a powerful decentralized rights-management framework that is flexible and scalable enough to suit the business needs of various markets & use-cases, including but not limited to the music industry, visual content, streaming, etc.

## Anagolay nodes

_Anagolay nodes_ or _Anagolay clients_ are peers that sync the Anagolay blockchain and validate the messages in every block, which, once applied generate a global state.

## Consensus

Since Anagolay Network is Substrate-based, it inherits the same consensus rules and governing functionalities. If you are interested in reading about it head [to the official Substrate docs](https://docs.substrate.io/).

### Idiyanale

Idiyanale will be launched in a Proof-of-Authority mode, and once the network is up and running with a sufficient number of well-staked validators, we will enable the Nominated Proof-of-Stake.
