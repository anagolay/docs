---
title: Macula
description: The Macula is a missing layer for IPFS with built-in features like API authentication, on-the-fly image processing, secure file storage, and hosting for modern SPA or statically built websites. Macula App enables users to encrypt and decrypt files using the Substrate-based accounts and store them on the IPFS.
---

| Name         | Value                                                              |
| ------------ | ------------------------------------------------------------------ |
| Repository   | https://github.com/kelp-hq/oss/tree/main/services/macula           |
| README.md    | https://github.com/kelp-hq/oss/tree/main/services/macula/README.md |
| SSPL LICENSE | https://github.com/kelp-hq/oss/tree/main/services/macula/LICENSE   |

The Macula is a missing layer for IPFS with built-in features like API authentication, on-the-fly image processing, secure file storage, and hosting for modern SPA or statically built websites. Macula App enables users to encrypt and decrypt files using the Substrate-based accounts and store them on the IPFS.

🏗️ The current set of features:

- Authenticated APIs using
  - API key strategy
  - web3 substrate-based account via Polkadot.js browser extension
- Access to IPFS native API on `macula.link/ipfs_api/`
- IPFS gateway served on `macula.link/ipfs/CID`
- Website hosting with CID or a subdomain
  - hosting specific version `https://bafybeiabw7h2j2wvpjzzi55oajj2yr3z7ifu2zfj2kpfefu2pulfljfx74.on.macula.link/`
  - hosting subdomain `https://test-website.macula.link/`
- SPA hosting with fully working refresh and html fallback page as a subdomain
- Statically build websites
  - if used with sveltekit Macula supported `precompress` feature and will serve Brotli compressed pages
- Gzip is enabled by default and Brotli only when requested
- highly efficient image processing based on Sharpjs via the URL search params
  - https://macula.kelp.digital/ipfs/bafybeic4fqttg2k6ibirqs57zcl3b2jd7hjnq22biyp542lfjhdyx5ausi/DSC02764.jpg?w=1000&h=400
- Ready-made `Caddyfile` with rewrites for non-docker environments
- Ready-made `docker-compose.yml` with rewrites for docker environment

with Macula user can have truly versioned websites and monitor how they performed and developed.

## Implemented
