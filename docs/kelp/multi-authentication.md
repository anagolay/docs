---
title: Auth scheme for Web3 and Web2
sidebar_label: 🔑 Multi authentication
tags:
  - web3
  - web2
  - authentication
  - authorization
---

Heavily inspired by JWT, MultiAuth is an authorization scheme for accessing the resources either on-chain ( in encrypted form ), on public or private IPFS ( may be encrypted ) or in any other database that is using the api-key or web3 based access.

:::info
WIP, docs are in progress, if you want to contribute or just get involved join our [discord](https://discordapp.com/invite/WHe4EuY) or [matrix](https://matrix.to/#/#anagolay-general:matrix.org) channel.
:::
