---
title: Remote sign anything using the PGP keys and Web3 accounts
sidebar_label: 🔐 Remote signer
tags:
  - git
  - remote
  - signer
  - PGP
  - web3
---

# Remote Signer

A client + app + service that bring signing of anything to the end user. Currently implemented and working are PGP git commit signing using web based VSCode.

:::info
WIP, docs are in progress, if you want to contribute or just get involved join our [discord](https://discordapp.com/invite/WHe4EuY) or [matrix](https://matrix.to/#/#anagolay-general:matrix.org) channel.
:::
