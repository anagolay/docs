---
title: IPFS CLI
sidebar_label: CLI
tags:
  - ipfs
  - cli
---

| Name         | Value                                                             |
| ------------ | ----------------------------------------------------------------- |
| Repository   | https://github.com/kelp-hq/oss/tree/main/tools/ipfs-cli           |
| README.md    | https://github.com/kelp-hq/oss/tree/main/tools/ipfs-cli/README.md |
| SSPL LICENSE | https://github.com/kelp-hq/oss/tree/main/tools/ipfs-cli/LICENSE   |

Ready to use CLI to retrieve, upload data to IPFS using the authentication methods. Built from scratch for ease of use.
