---
title: Prove Domain Ownership
description: Prove domain ownership and store the proofs on the Anagolay chain
sidebar_label: Prove Domain Ownership
tags:
  - proofs
  - anagolay
  - domain
  - video
---

Here is the video we made that shows how it is done.

<iframe width="100%" height="415" src="https://www.youtube.com/embed/zjs2m0hwU8k" title="Anagolay Demo, Prove your domain ownership" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen={true}></iframe>

---

:::info
I hope you will enjoy it, and if you do, don't forget to follow and like.
:::
