---
title: Tipping
description: Tipping allows to create proof of ownership of domains and social accounts.
authors: [daniel, adriano]
tags:
  - tipping
  - community_support
  - anagolay
  - verification
  - ownership
  - account
---

| Name                              | Value                                                                                |
| --------------------------------- | ------------------------------------------------------------------------------------ |
| Repository                        | https://github.com/anagolay/anagolay-chain/tree/main/pallets/tipping                 |
| Pallet                            | Yes                                                                                  |
| License                           | [AGPL](https://github.com/anagolay/anagolay-chain/tree/main/pallets/tipping/LICENSE) |
| MAX_TIPS_PER_VERIFICATION_CONTEXT | `10000`                                                                              |

# Tipping

## Overview

`The tipping pallet` will enable sending the community contributions (tips) to verified creators currently in the Anagolay native tokens and later, using fiat and many Dotsama ecosystem tokens. To prevent the misuse of the pallet and to make sure that the correct people are supported, the tipping pallet depends on the `verification` pallet to get the proofs of the domain, username or repository verification, and `statements` pallet for the ownership.

Tipping pallet is the core feature in our attempt to build the functionality and features to support creators’ economy in a truly decentralized manner.

Every creator can verify their revenue channels like websites, subdomains, or a username on commercial websites and accept payment from anybody in crypto.

Sending a tip to a verified online identity:

```mermaid
sequenceDiagram
    actor Alice
    Extension->>IdiyanaleChain: Check for verification
    IdiyanaleChain->>Extension: Domain verified
    Note over Extension: Enable tipping functionality
    Alice->>+Extension: Select native token for tipping (IDI)
    Alice->>+Extension: Select the amount
    Extension->>+IdiyanaleChain: Check funds for the account
    IdiyanaleChain-->>Extension: Funds amount
    Note over Extension: Enable tipping button if Funds > Tip amount
    Alice->>+Extension: Send tip
    Extension->>+IdiyanaleChain: Invoke the tip() extrinsic
    IdiyanaleChain-->>Extension: Success
    Extension-->>Alice: Tip successful
    Note over Alice,Extension: Confetti 🥳

```

## Configuration

The runtime needs to configure the tipping pallet as follows:

```rust
  impl tipping::Config for Runtime {
    type Event = Event;
    type Currency = Balances;
    type TimeProvider = pallet_timestamp::Pallet<Runtime>;
    type WeightInfo = tipping::weights::AnagolayWeight<Runtime>;

    /// Limit on the maximum number of tips that will be recorded, per context.
    /// This only affects the showing of the last X transactions when rpc is called
    const MAX_TIPS_PER_VERIFICATION_CONTEXT: u32 = 10000;
  }
```

There is one constant available for configuration:

- `MAX_TIPS_PER_VERIFICATION_CONTEXT`: specifies the amount of tips which will be recorded per verification context. Once reached this limit, the older tips are discarded. Does not affect receiving tips, and it is used to show last X of the transactions when the `getTips` RPC is called

## Storage

- `TippingSettingsByAccountIdAndVerificationContext` The map of `TippingSetting`s indexed by their respective `AccountId` and `VerificationContext`
- `TipsByAccountIdAndVerificationContext` Provides a collection of `Tip`s indexed by their respective receiver `AccountId` and `VerificationContext`

## Events

- `TippingSettingsUpdated` produced upon tipping settings update
- `TipCreated` produced upon the newly created tip

## Errors

- `InvalidVerificationContext` happens when verification context is not associated to a successful verification request and cannot be tipped
- `InvalidConfiguration` happens when trying to tip a verification context is not set-up to enable tipping

## Types

- `TippingSettings` is a structure associated with `VerificationContext`, providing the tipping settings
  - `context`: the verification context this setting applies to
  - `enabled`: specifies that the tipping is enabled or not
  - `account`: specifies to which wallet to send the tips given in some kind of token
- `Tip` structure representing a tip, providing the following fields:
  - `amount` - quantity of tokens tipped
  - `sender` - the `AccountId` that is tipping
  - `sender` - the `AccountId` that is receiving the tip
  - `createdAt` - timestamp of the tip, in seconds
  - `blockNumber`: block where the tip was inserted

## Extrinsic

1. `update_settings` accepts a collection of `TippingSettings` and stores it. A coherency check is performed on the rightfulness of the caller to configure each setting. Infact, the single setting won't be stored if the caller is not the holder of the verification request for the respective context, or if such request has a status different from `Success`. In the end, a `TippingSettingsUpdated` event is raised
2. `tip` accepts a `Tip` for a `VerificationContext` and stores them in the `TipsByAccountIdAndVerificationContext` while it transfers of the required amount from the account of the sender to the account of the receiver.

## RPCs

### `getTips`

```rust
/// Get the tips for an Account and a [`VerificationContext`]
///
/// # Arguments
///  * account_id - The account to query
///  * verification_context - The [`VerificationContext`] to query
///  * offset - The index, inside the ids set, of the first Tip on the page
///  * limit - The count of Tips on the page
///  * sort - Specifies the order in which tips are sorted by their created_at field
///
/// # Return
/// Collection of [`Tip`]s
fn get_tips (
  account_id: AccountId,
  verification_context: VerificationContext,
  offset: u64,
  limit: u16,
  sort: TippingSort,
) -> Vec<Tip<Balance, AccountId, BlockNumber>>;
```

### `total`

```rust
/// Get the count of tips for a [`VerificationContext`]
///
/// # Arguments
///  * account_id - The holder of a successful [`VerificationRequest`] for the verification context
///  * verification_context - The [`VerificationContext`] to query
///
/// # Return
/// Count of [`Tip`]s for the specified verification context
fn total(
  account_id: AccountId,
  verification_context: VerificationContext
) -> u64;
```

### `total_received`

```rust
/// Get the total balance of tips received for a [`VerificationContext`]
///
/// # Arguments
///  * account_id - The holder of a successful [`VerificationRequest`] for the verification context
///  * verification_context - The [`VerificationContext`] to query
///
/// # Return
/// Total balance, sum of all [`Tip`]s for the specified verification context
fn total_received(
  account_id: AccountId,
  verification_context: VerificationContext
) -> Balance;
```
