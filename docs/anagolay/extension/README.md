---
title: Anagolay Web Extension
description: Anagolay Extension has few unique features that no other extension in Dotsama eco-system has.
tags:
  - tipping
  - community_support
  - anagolay
  - verification
  - ownership
  - account
  - extension
  - chrome
---

# Another wallet?

:::tip
This page is WIP.
:::

Well, yes and no. Currently, the extension acts as a wallet, holding private keys. That will change in the future.

Anagolay Extension has few unique features that no other extension in Dotsama eco-system has.

Here is the list:

## Tipping

In short, everybody can send support in a form of Tokens either Anagolay native or not, to any verified creator.

If you are a developer, you can check the [tipping pallet](../tipping.md) and [verification pallet](../verification.md).

---

If you are interested in this feature come and join us on [Discord](https://discordapp.com/invite/WHe4EuY).
