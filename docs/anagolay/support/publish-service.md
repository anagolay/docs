---
title: Publish service
description: Microservice that builds operations and workflows
tags:
  - anagolay
  - wasm
  - rust
  - nostd
---

| Name       | Value |
| ---------- | ----- |
| Repository | -     |
