---
title: Anagolay CLI
description: Create, manage and publish operations and Workflows to Anagolay network
tags:
  - operation
  - anagolay
  - deterministic
  - wasm
  - rust
  - nostd
---

| Name       | Value                                                         |
| ---------- | ------------------------------------------------------------- |
| Repository | https://github.com/anagolay/anagolay-js-sdk/tree/main/sdk/cli |
