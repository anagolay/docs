---
title: IDI Token
sidebar_label: IDI
tags:
  - tokens
  - idi
  - utility
---

Initially, the Idiyanale chain is intended as a playground for developers, where they can try and test both new and existing features of Anagolay Network in an unrefined, unpredictable but realistic on-chain environment. This implies that Idiyanale:

- will serve as a warning system for Anagolay mainnet for any issues that may threaten the stability of the network.
- allows the most realistic testing environment possible for projects aiming to build on Anagolay in the future.
- in principle, is the same as Anagolay Network and follows the same structure.

Additionally, Idiyanale Network will enable and test the establishment of a practical, creative economy where the work of all contributors is encouraged and rewarded. This creative market economy shall emerge naturally and will be accommodated by Anagolay Network later on. It is important to note that the design mechanisms discussed here express the team’s current hypothesis and views on this topic. The purpose of Idiyanale as a Pioneer network of Anagolay is to test, validate and improve on these mechanisms. The full potential of the Idiyanale and Anagolay token economy is subject to change based on ongoing research.

Idiyanale will be launched in a Proof-of-Authority mode, and once the network is up and running with a sufficient number of well-staked validators, we will enable the Nominated Proof-of-Stake.

Idiyanale is powered by IDI tokens, which act as means of payment between various actors in the Anagolay ecosystem, payment of the fees for the on-chain storage of Operations and Workflows, Network governance, and staking to secure the network.

:::info
More on the [token utility here](./idi-metrics.md).
:::
