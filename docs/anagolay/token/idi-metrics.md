---
title: IDI Token Metrics
tags:
  - tokens
  - agy
  - idi
  - metrics
  - tge
---

**Idiyanale Network** will enable experiments in the establishment of a practical, creative economy where the work of all contributors is encouraged and rewarded. This creative market economy shall emerge naturally and will be accommodated by Anagolay Network later on. It is important to note that the design mechanisms discussed here express the team’s current hypothesis and views on this topic. The purpose of Idiyanale as a Pioneer network of Anagolay is to test, validate and improve on these mechanisms. The full potential of the Idiyanale and Anagolay token economy is subject to change based on ongoing research.

The IDI token is required to perform certain actions within the Idiyanale Network. The issuance of the initial tranches of IDI tokens is done by Kelp Digital OU. From the moment of the mainnet launch, the network incentivization mechanisms and token functionality will facilitate IDI distribution to the community groups, with them gradually taking over the network governance.

**The overall functionality of IDI tokens can be broken down into the following:**

### Payment Utility

Users of the Anagolay Protocol will be able to use IDI tokens (as well as AGY tokens later on) as payment for usage of the network functionality either directly or via Dapps. Paying Storage fees for the issued statements, acquiring licenses for others' content, and tipping other creators to support their work in general or one particular cause are just a few examples the team is already working on today. More examples of payment utility will be developed with the development of ecosystems and the emergence of new Dapps and integrations.

- Means of transactions for Dapps users (ex. buy, license content)
- In-browser Tipping Functionality for verified domain/account owners
- Storage fees for copyrights and ownership data via Dapps (ex. Kelp)

### On-Chain Utility

IDI is an incentive, means of regulation and transactions for various actors on the Anagolay Network: from the fees for the on-chain storage of Operations and Workflows to Network governance mechanisms.

- Storage of Operations & Workflows on the chain
- Community rewards for verifying and approving new operations
- Network Governance: voting on network decisions and upgrades

### Staking Utility

Idiyanale is set out as a delegated proof-of-stake based blockchain network. This means that the validators would need to stake IDI tokens to be able to take part in the validation process. This requirement for staking would further increase the demand for IDI tokens.

- Staking to secure and support the network, produce blocks and verify transactions
- Token holders will be able to vote for their choice of validators to receive staking rewards
- The functionality map of the IDI tokens associated with various network actors is shown here:

![IDI & AGY Token Utility Chart](/assets/tokens/idi_agy_token_utility_chart.png)

IDI & AGY Token Utility Chart

## Token Issuance and Allocation

**Total supply at TGE**: 1B (**1,000,000,000**) pre-minted IDI tokens

- Initial circulating supply: **175 million -** Only 17,5% of the total number of tokens minted at TGE will be in circulation initially.
- The remaining pre-minted tokens will be gradually unlocked over a period of **6 to 48** months.
- Initial inflation will be set at 10% per annum and then adjusted following the staking rate.
- After TGE, newly minted tokens are paid out to collators, delegators and the
  Treasury.
- Subsequently Anagolay Network will be launched with **all the essential data from Idiyanale in its genesis block.** This includes all Anagolay-specific functionality such as Operations, Workflows, Artifacts, PoEs, Statements, as well as the percentage of the **initial token distribution to IDI tokens holders** (who will be entitled to AGY token share respective to their IDI tokens).

### Initial IDI Allocation Split

At the TGE there will be 1B pre-minted IDI Tokens. All of these tokens,
regardless of being unvested, vested or locked, can be used (not spent, but counted as a voting power) in some governance and voting processes.

At genesis, the $IDI token will be allocated as follows:

![Chart 1 - IDI initial token allocation](/assets/tokens/idi_initial_token_allocation.png)

Chart 1 - IDI initial token allocation

- 28%, of IDI tokens are allocated to the early backers and community pioneers
- 12% of IDI tokens are allocated to Kelp.Digital team & board of advisors with 36-month linear vesting.
- 45% of tokens initially locked in treasury and will be relished via liner vesting quarterly over the next 24 months to provide incentives for:
  - development of the framework & ecosystem (additional use cases & dapps)
  - Incentives for collators & validators pool and future staking rewards
- Another 12% are dedicated to community building & growing initiatives
- Finally, 3% are pre-reserved for en exchange launch pool

## Token sales

**Total Amount we aim to raise: $9.3M at fully deluded valuation $58M**

There will be 2 private rounds and a pre-sale event for the community pioneers, before IDI will become available to the general public.

[IDI Token Rounds](https://www.notion.so/097ec6410b4a45a1b4678463c8c4856d)

- **1st Private Private** (Institutional investors) - 90,000,000 $IDI
  - 15% no lockup and no vesting, released at TGE
  - 85% vested over 2 years with equal monthly unlocks
- **Private round** (Institutional investors & Community) - $5,200,000
  - 25% no lockup and no vesting, released at TGE
  - 75% vested over 2 years with equal monthly unlocks
- **Pre-sale for community pioneers:**
  - 40% no lockup and no vesting, released at TGE
  - 60% - 6 months ramp (cliff) & then equal monthly release (18 months vesting)
- **Public sale at TGE:**
  - 40% no lockup and no vesting, released at TGE
  - 60% - 6 months ramp (cliff) & then equal monthly release (18 months vesting)

### Vesting Schedules

![Chart 2 - current proposal, subject to change.](/assets/tokens/vesting-schedules.png)

## On the Network Succession Principle

In Anagolay Framework Idiyanale is the 1st of the two main chains delivered consecutively under the **succession principle.**

The succession principle implies that once the core features of the Framework are well-tested and approved by the community Anagolay Network will inherit the role of the main net from Idiyanale, receiving all the relevant data & value as a kick-start capital, recorded in its genesis block, including:

- Operations, Workflows & Artefacts
- Previously created Statements & Proofs
- % IDI Token

![Build -> test -> pass](/assets/tokens/build_test_pass.png)

IDI Token Holders will be entitled to receive a number of AGY tokens, respective to their IDI share. The % of IDI tokens holders included in the Anagolay genesis block.

IDI stakers potentially can directly become nominated validators on Anagolay Network while keeping their role and stake on Idiyanale.

After the Anagolay launch, Idiyanale will either become a testnet for the upcoming features on Anagolay, or take a different path voted by the community.

## Staking Rewards

- While at it’s launch Idiyanale Network is governed by PoA, later on, the delegated proof of Stake consensus will be implemented - current estimation Q2 2023. Validator rewards will be enabled on mainnet - tokens are liquid for use on the network as soon as they are withdrawn by the node operator.
- 100% of inflation is distributed to stakers and treasury – meaning holders of IDI (and later on AGY) will not be diluted as long as stakers are actively participating. In other words, new issuance is only distributed to validators staking and performing work to support the network, or validators directly pledging their tokens against a specific validator’s dependability.
- The expected staking percentage is expected to decline over time as additional use cases become available. As this occurs, Anagolay is designed to minimize inflation and will optimize this against providing sufficient rewards for validators. New issuance is offset by the fees collected by the network. Because of this, high levels of transaction throughput result in lower annual issuance.

## Fees computation and destination

There are several reasons to introduce fees on transactions performed on the chain, namely to recover operational costs, discourage harmful behaviors, and handle eventual peaks of activity and long inclusion times.

The final fee for a transaction is a function of its length (size in bytes), a linear combination of the following parameters:

- _base fee_: This is the minimum amount a user pays for a transaction. It is related to a preconfigured base weight and converted to a fee using a constant multiplier.
- _weight fee_: A fee proportional to the execution time (input and output and computation) that a transaction consumes.
- _length fee_: A fee proportional to the encoded length of the transaction. It’s a polynomial function composed of the sum of two curves: a linear curve, negligible for lengthy transactions, and a cubic curve that, on the contrary, grows fast for lengthy transactions
- _tip_: An optional tip to increase the priority of the transaction, giving it a higher chance to be included by the transaction queue.

Due to the irregular nature of the demand for transactions on the blockchain, a fee adjustment mechanism is in place: by the law of supply and demand, raising the fee should decrease the demand and vice-versa. The variation takes into consideration the long period and doesn’t discourage temporary spikes of activity.

The collected fees are partitioned as follows: block producers receive the entirety of the tip so that there is the possibility to prioritize the inclusion of the transaction in the block even in adverse network conditions. Along with the tip, they receive 20% of the computed fee.

The remaining part is routed to the treasury; however, in order to moderate inflation, 20% of this remaining part is accredited to the treasury while 80% is burnt.
