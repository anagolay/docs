---
title: AGY Token
sidebar_label: AGY
tags:
  - tokens
  - agy
  - utility
---

Anagolay Network will be launched with **all the essential data from Idiyanale in its genesis block.** This includes all Anagolay-specific functionality such as Operations, Workflows, Artifacts, PoEs, and Statements, as well as the percentage of the **initial token distribution to IDI tokens holders** (who will be entitled to AGY token share respective to their IDI tokens).

Notably, the Succession Principle can be similarly used for Anagolay network in the future if the need arises. A genesis file can be generated from the Anagolay chain hosted anywhere on Earth and become initial data, in any other isolated environment, even on Mars, for example. A different scenario - creating a single backup in highly durable storage in case of any possible disaster; or making sure the data stays available regardless of the restrictions imposed on certain territories by a centralized power.

AGY token will enable the functionalities of Anagolay Framework and its Dapps and create incentives for various actors/user groups: builders community (creation and storing of Workflows and Operations), Dapp end-users (creating and storing Statements, managing & transferring rights, tipping, etc.).

:::info
More on the [IDI token utility here](./idi-metrics.md).
:::
