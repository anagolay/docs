---
title: Accounts
description: How to create Accounts, the specification and minimum deposit
---

The Anagolay account is the substrate-based account that is ultimate owner of any asset on Anagolay networks. Users must keep accounts' mnemonic seed and passphrase safe and follow the best-practices in sharing and importing them. Anagolay accounts are essentially substrate accounts and thus compatible with all substrate-based blockchains. Here is the [in-depth docs on accounts](https://docs.substrate.io/fundamentals/accounts-addresses-keys/), feel free to check it out.

We have decided that we will not change address encoding from the default setting because we don't want to confuse users with additional information that suits only the building the brand.

Here are the defaults and some account settings:

| Name                | Value    |
| ------------------- | -------- |
| Existential deposit | 0,01 IDI |
| Prefix              | 42       |
| Decimals            | 12       |

## How to create account?

Any wallet that supports Substrate based blockchains can be used to create Anagolay account. The simplest way is to use the Polkadot-js browser extension.

:::info

The rest of the page is work in progress, in the meantime you can check official docs how to create substrate based accounts blow

:::

Resources:

- https://docs.substrate.io/fundamentals/accounts-addresses-keys/
